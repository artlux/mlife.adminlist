<?
IncludeModuleLangFile(__FILE__);

class mlife_adminlist extends CModule
{
	var $MODULE_ID = "mlife.adminlist";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;

	function mlife_adminlist()
	{
		$arModuleVersion = array();
		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->PARTNER_NAME = GetMessage("MLIFE_COMPANY_NAME");
		$this->PARTNER_URI = "http://mlife-media.by/";
		$this->MODULE_NAME = GetMessage("MLIFE_ADMINLIST_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("MLIFE_ADMINLIST_DESCRIPTION");
		return true;
	}
	
	function InstallDB($arParams = array())
	{
		return true;
	}
	
	function UnInstallDB($arParams = array())
	{
		return true;
	}
	
	function InstallEvents()
	{
		return true;
	}
	
	function UnInstallEvents()
	{
		return true;
	}
	
	function InstallFiles($arParams = array())
	{
		CopyDirFiles(
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.adminlist/install/admin",
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
		return true;
	}
	
	function UnInstallFiles()
	{
		DeleteDirFiles(
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.adminlist/install/admin",
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
		return true;
	}
	
	function DoInstall()
	{
		$this->InstallFiles();
		RegisterModule("mlife.adminlist");
	}
	
	function UnInstallFiles()
	{
		$this->InstallFiles();
		UnRegisterModule("mlife.adminlist");
	}

}
?>